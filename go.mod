module gitlab.com/ddb_db/xtreamhome

go 1.21

require golang.org/x/sync v0.1.0 // indirect

require (
	github.com/gorilla/mux v1.8.1
	github.com/inhies/go-bytesize v0.0.0-20220417184213-4913239db9cf
	github.com/jellydator/ttlcache/v3 v3.1.0
	gopkg.in/yaml.v3 v3.0.1
)
