package main

import (
	"context"
	"flag"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"strings"

	"gitlab.com/ddb_db/xtreamhome/internal/config"
	"gitlab.com/ddb_db/xtreamhome/internal/provider"
	"gitlab.com/ddb_db/xtreamhome/internal/router"
	"gitlab.com/ddb_db/xtreamhome/internal/users"
	"gitlab.com/ddb_db/xtreamhome/internal/workers/refresh"
)

func main() {
	cfg := parseConfig()
	configLogger(cfg.Config.LogLevel)

	ctx := context.Background() // TODO Need some type of cancel context/signal handler, etc.
	refreshUsers := make(chan refresh.RefreshMessage)

	upstreamRefresh := make(chan refresh.RefreshMessage)
	go refresh.NewProviderRefresher(ctx, cfg.Provider.Refresh)(upstreamRefresh)

	provider.New(config.ActiveConfig.Provider.Username, cfg.Provider.Password, cfg.Provider.URL, upstreamRefresh)
	provider.ActiveProvider.Init(ctx, refreshUsers)

	users.New(refreshUsers)
	users.ActiveUsers.Init(ctx, cfg.Users, provider.ActiveProvider)

	http.Handle("/", router.New())
	http.ListenAndServe(fmt.Sprintf("%s:%d", cfg.Config.BindAddr, cfg.Config.Port), nil)
}

func configLogger(lvl string) *slog.Logger {
	h := slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		Level: strtolvl(lvl),
	})
	return slog.New(h)
}

func parseConfig() config.Config {
	configFile := "settings.yaml"
	flag.StringVar(&configFile, "config", configFile, "path to the configuration yaml file")
	flag.Parse()
	return *config.New(configFile)
}

func strtolvl(lvl string) slog.Leveler {
	switch strings.ToLower(lvl) {
	case "trace":
		return slog.LevelDebug
	case "debug":
		return slog.LevelDebug
	case "info":
		return slog.LevelInfo
	case "warn":
		return slog.LevelWarn
	case "error":
		return slog.LevelError
	default:
		panic(fmt.Errorf("invalid log level configured: [%s]", lvl))
	}
}
