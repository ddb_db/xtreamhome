# Xtream Home: Personal Xtream IPTV Proxy
Xtream Home is a ~simple~ simple-ish proxy for Xtream based IPTV services.  Manage custom
playlists for yourself, friends and family.

## PoC (Proof of Concept) #2
Where PoC 1 just proved that I could do the basics, PoC 2 turned into something a little bit bigger than I was expecting.
I managed to find some free time over the holidays and engineered something a little bit bigger than I had planned for. :)
Most of the new features are basically what I was aiming for, just a little more adavanced/flexible than I originally
thought of.  But the use of these features is optional.  The very basic use case is still simple to setup with a minimal
yaml config file.  

![image](xthome.png)
* A single, self contained binary along with a single SQLite database file.
* A low powered CPU and minimum 2GBs of RAM (4GB recommended if your IPTV provider has >5000 channels)
  * The more powerful the CPU, the quicker XML and JSON processing can be done but the RAM is non-negotiable,
    if you have lots of channels then the xml epg data will require lots of memory to parse.
* Define multiple IPTV providers with multiple logins per provider.
  * Your users are then granted access to 1 or more providers and automatically share the pooled providers/logins
  * If you need to change providers and/or modify provider logins, you **don't** need to change your users' logins/devices.
  * Provider data is cached locally and refreshed periodically.
  * All provider data is optionally filtered through JavaScript filters that can filter, modify, or enhance data received.
    * For example, if one provider has great EPG data and another has poor or no EPG, then you can relink the EPG ids so
      both providers end up sharing EPG data.  _(Very, very doable, but does require some JS skills and some advanced scripting
      to achieve.)_
* Define multiple EPG data sources.
  * EPG sources are separated from stream data.
  * EPG data can be filtered through JavaScript filters. (**Note:** EPG filters not fully implemented yet.)
  * Any provider can use any EPG data source as long as the epg ids in the streams match an id in the epg data.
    * This is where you'd use provider filters to remap epg ids as needed.
* Create custom users with per user filtered groups
  * Each user can be customized via JavaScript; these filters can be as simple or as complicated as needed.
  * You can also create custom categories per user that combine streams from multiple providers.
  * The JS filters are optional; if you don't define them for a user, the user just gets every category from
    the granted provider(s) made available to them.
    * But it's the JS filtering mechanism that provides the power of XtreamHome.  It's these filters that let you do
      all sorts of interesting things **on the server side**, which means these categories, etc. that you create for
      a user automatically carry to multiple devices for that user.
    * Some interesting things I've done so far on a per user basis:
      * Created a favourites group with my favourite channels setup.  Now when I setup a new device in the house or add
        my account to a phone, that group just automatically shows up.
      * Created groups with channels from multiple sources.
        * The game isn't playing on one provider's feed, just scroll through the group and try other sources of the same channel.
      * Dynamic search groups
        * Setup groups with key words and when those key words are found in the EPG, a new category is created with the matching
          channels.  When there are no matches, the category disappears on the next playlist update.  I'm using it for sports where
          I have filters setup for everyone's favourite teams.  On game days, the category shows up with multiple channels that
          are (likely) playing the game -- from multiple providers.  When the team isn't playing, the category disappears for the day.
      * All of these things are implemented with JavaScript filters.  The JS engine injects various objects into your script (provider
        data, stream data, epg search functions, etc.) and then you just write a script to manipulate the user's categories, etc.
        and return the result.  XtreamHome then saves the result to its database, which is then used to generate the user's XtreamCode
        API responses when requested.  Again, the JS filters can be very simple or as complicated as you need them to be.  The
        flexibility a true scripting engine provides means the possibilities are almost endless.
* Each user has a generated xmltv epg that only contains the channels within their filtered groups
  * Users don't need to download and process xmltv data for channels they don't have access to.
* Upstream Xtream API and EPG data are cached for fast response times and refreshed periodically
* All stream playback is redirected to upstream provider
* All configuration done within a yaml file
* Since users auth with this server, no need to reconfigure clients when changing upstream provider
* Since groups are managed server side, filtered groups carry across IPTV client installations!
  * No need to reconfigure custom groups, disable groups, etc. on each client device!
* Tested against Tivimate and IPTVSmartersPro

## Ok, this sounds great, where's the code/download?
The code needs some work before I'm ready to post it here.  It needs enough work that I want to
gauge interst in this project to decide how I clean things up before publishing the code.  These
PoCs were little side projects over the holidays and though I've got something that suits my needs,
it's not something that's quite ready for publishing.  Until something is published, checkout the
main branch for PoC 1, which can be built and run.  But be warned that the config, etc. between the
PoCs are vastly different and not compatible.  The way things work is also very different between
the two PoCs.

## Who is this for?
Are you the person who makes sure everyone's Tivimate is working properly?
Are you the only person who even knows what Xtream IPTV is?  Do you hate
having to constantly reconfigure multiple Tivimate installs for people?
If you answered yes to any of the above then Xtream Home might be for you.

Xtream Home provides an Xtream IPTV server that proxies for your upstream
providers.  You configure logins for friends and family and they're set 
even if you need to make a provider change.  You can create filtered
category lists per user and Xtream Home creates a pruned xmltv epg file
based on the filtered lists you've assigned to each user.

### Don't Expose to Internet
**It is highly recommended not to expose the current version to the Internet.**
The current version is a PoC (proof of concept) only!  Test it on your LAN/home
lab but this is not even close to being ready for public exposure (and it may
never be).

I am using the current version of the code successfully **but**:

* I run it locally in my LAN, it is **not** exposed to the Intenet
* Friends/family connect to it over VPN

## Who is this NOT for?
This app is not designed for scale.  The target user is someone who's
managing an iptv service for a few friends and family and wants some
customization.  You will need to deploy this app either in the cloud
via Digital Ocean, AWS, etc. or run it locally and grant access to the
people who need to access it.

The PoC is configured completely via a yaml file.  There is no GUI nor
is one planned on the immediate road map.  The target user has 2-4 friends
and family and doesn't mind spending some time writing a yaml file to get
things going.

## Current State: PoC (proof of concept)
I am running PoC 2 in my home lab and have friends and family
successfully connecting to it.  PoC 2 turned into something much larger than
PoC 1.  It's working, it does what I want and if there's no other interest in
this project by others, I'll probably leave it pretty much as is and only
address bugs I find and some quality of life improvements over time.

## Road Map
The road map is based on the features I need/want before having my
friends and family start to connect to it.

* Refactor the PoC code; prep the code for futher development
* ~Regex based group filtering~ **Done, via JS filter engine**
* ~Filtering of channels within groups~ **Done, via JS filter engine**
* ~Creating of custom groups whose channels are sourced from regex matching~ **Done, via JS filter engine**

After the above are addressed, I'd feel ready to get the friends and family
connecting to it.

* Upstream change notifications (groups added/removed, etc.)
* Diagnostic tools (cli) **Partially done.**
* ~Dynamic custom groups (more details to come later)~ **Done, via JS filter engine**

The above completes the key features that motivated me to start this project.
I'll be really happy when I get to this point.

* ~Support for multiple upstream providers (and all the complications that brings)~ **Done.**
* ~Support for reordering of channels within a group~ **Done, via JS filter engine**
* Support for https/tls -- or maybe just a howto on configuring an nginx reverse proxy ;)
* Web based GUI configuration
