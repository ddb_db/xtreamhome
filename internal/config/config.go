package config

import (
	"bytes"
	"log/slog"
	"net"
	"os"
	"time"

	"github.com/inhies/go-bytesize"
	"gopkg.in/yaml.v3"
)

type ServerConfig struct {
	Hostname    string `yaml:"hostname"`
	Port        int    `yaml:"port"`
	Proto       string `yaml:"proto"`
	BindAddr    string `yaml:"bindaddr"`
	Timezone    string `yaml:"timezone"`
	LogLevel    string `yaml:"loglevel"`
	LogReqs     bool   `yaml:"reqlog"`
	EnablePprof bool   `yaml:"pprof"`
}

type ProviderConfig struct {
	ID       string            `yaml:"id"`
	Name     string            `yaml:"name"`
	URL      string            `yaml:"url"`
	Username string            `yaml:"username"`
	Password string            `yaml:"password"`
	Refresh  time.Duration     `yaml:"refresh"`
	Cache    bytesize.ByteSize `yaml:"cache"`
	CacheTTL time.Duration     `yaml:"cachettl"`
}

type UserGroupConfig struct {
	Name   string `yaml:"name"`
	Rename string `yaml:"rename"`
}

type UserConfig struct {
	Username     string            `yaml:"username"`
	Password     string            `yaml:"password"`
	LiveGroups   []UserGroupConfig `yaml:"live"`
	SeriesGroups []UserGroupConfig `yaml:"series"`
	VODGroups    []UserGroupConfig `yaml:"vod"`
}

type Config struct {
	Config   ServerConfig   `yaml:"config"`
	Provider ProviderConfig `yaml:"provider"`
	Users    []UserConfig   `yaml:"users"`
}

var ActiveConfig *Config

func New(path string) *Config {
	if ActiveConfig != nil {
		panic("config already initialized")
	}

	f, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return NewFromBytes(f)
}

func NewFromBytes(data []byte) *Config {
	if ActiveConfig != nil {
		panic("config already initialized")
	}

	d := yaml.NewDecoder(bytes.NewReader(data))
	defaultCache, _ := bytesize.Parse("500mb")
	defaultHostname, _ := os.Hostname()
	var defaultBindAddr any
	defaultBindAddr, err := net.LookupIP("localhost")
	if err != nil {
		slog.Warn("config: dns error for localhost", "err", err.Error())
		defaultBindAddr = "127.0.0.1"
	} else {
		defaultBindAddr = defaultBindAddr.([]net.IP)[0].String()
	}

	cfg := Config{
		Config: ServerConfig{
			Hostname:    defaultHostname,
			Port:        3000,
			Proto:       "http",
			Timezone:    "UTC",
			BindAddr:    defaultBindAddr.(string),
			LogLevel:    "info",
			LogReqs:     false,
			EnablePprof: false,
		},
		Provider: ProviderConfig{
			Cache:    defaultCache,
			Refresh:  time.Hour * 4,
			CacheTTL: time.Minute * 30,
		},
	}
	if err := d.Decode(&cfg); err != nil {
		panic(err)
	}
	ActiveConfig = &cfg
	return ActiveConfig
}
