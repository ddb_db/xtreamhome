package refresh

type Refresher func(RefreshSignalSender)
type RefreshSignalSender chan<- RefreshMessage
type RefreshSignalReceiver <-chan RefreshMessage
type RefreshSignal chan RefreshMessage
type RefreshMessage struct{}
