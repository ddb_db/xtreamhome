package refresh

import (
	"context"
	"log/slog"
	"time"
)

func NewProviderRefresher(ctx context.Context, wait time.Duration) Refresher {
	return func(s RefreshSignalSender) {
		for {
			select {
			case <-ctx.Done():
				slog.Info("refresh(provider): terminated", "cause", ctx.Err().Error())
				return
			case <-time.After(wait):
				s <- RefreshMessage{}
				slog.Info("refresh(provider): provider refresh triggered")
			}
		}
	}
}
