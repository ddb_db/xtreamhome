package users

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/ddb_db/xtreamhome/internal/config"
	"gitlab.com/ddb_db/xtreamhome/internal/provider"
	"gitlab.com/ddb_db/xtreamhome/internal/workers/refresh"
)

func NewUser(username, password string,
	liveCats []provider.LiveCategory,
	liveStreams map[string][]provider.LiveStream,
	vodCats []provider.VODCategory,
	vodStreams map[string][]provider.VODStream,
	seriesCats []provider.SeriesCategory,
	seriesStreams map[string][]provider.SeriesStream,
	epg []byte,
	upstream provider.Provider,
) User {
	return &defaultUser{
		username:         username,
		password:         password,
		liveCats:         liveCats,
		liveStreams:      liveStreams,
		liveStreamsCount: countMapVals(liveStreams),
		vodCats:          vodCats,
		vodStreams:       vodStreams,
		vodCount:         countMapVals(vodStreams),
		seriesCats:       seriesCats,
		seriesStreams:    seriesStreams,
		seriesCount:      countMapVals(seriesStreams),
		epg:              epg,
		upstream:         upstream,
	}
}

type defaultUser struct {
	username         string
	password         string
	liveCats         []provider.LiveCategory
	vodCats          []provider.VODCategory
	seriesCats       []provider.SeriesCategory
	liveStreams      map[string][]provider.LiveStream
	liveStreamsCount uint64
	vodStreams       map[string][]provider.VODStream
	vodCount         uint64
	seriesStreams    map[string][]provider.SeriesStream
	seriesCount      uint64
	epg              []byte
	upstream         provider.Provider
}

func (u *defaultUser) Init(ctx context.Context, listeners ...refresh.RefreshSignalSender) error {
	for _, l := range listeners {
		l <- struct{}{}
	}
	return nil
}

func (u *defaultUser) EPGXML() (*provider.XMLTV, error) { return nil, nil }

func (u *defaultUser) Auth() (provider.ServiceDetails, error) {
	sd, err := u.upstream.Auth()
	if err != nil {
		return provider.ServiceDetails{}, fmt.Errorf("users(default): upstream error: %w", err)
	}
	sd.UserInfo.Username = u.username
	sd.UserInfo.Password = u.password
	sd.ServerInfo = provider.ServerDetails{
		URL:            config.ActiveConfig.Config.Hostname,
		Port:           config.ActiveConfig.Config.Port,
		ServerProtocol: config.ActiveConfig.Config.Proto,
		RTMPPort:       "0",
		Timezone:       config.ActiveConfig.Config.Timezone,
		NowTimestamp:   uint64(time.Now().Unix()),
		Now:            time.Now().Format(time.RFC3339),
		Process:        true,
	}
	return sd, nil
}

func (u *defaultUser) LiveCategories() ([]provider.LiveCategory, error) {
	return u.liveCats, nil
}

func (u *defaultUser) VODCategories() ([]provider.VODCategory, error) {
	return u.vodCats, nil
}

func (u *defaultUser) SeriesCategories() ([]provider.SeriesCategory, error) {
	return u.seriesCats, nil
}

func (u *defaultUser) LiveStreams(id ...string) ([]provider.LiveStream, error) {
	if len(id) > 0 {
		return u.liveStreams[id[0]], nil
	}
	result := make([]provider.LiveStream, 0, u.liveStreamsCount)
	for _, v := range u.liveStreams {
		result = append(result, v...)
	}
	return result, nil
}

func (u *defaultUser) VODStreams(id ...string) ([]provider.VODStream, error) {
	if len(id) > 0 {
		return u.vodStreams[id[0]], nil
	}
	result := make([]provider.VODStream, 0, u.vodCount)
	for _, v := range u.vodStreams {
		result = append(result, v...)
	}
	return result, nil
}

func (u *defaultUser) SeriesStreams(id ...string) ([]provider.SeriesStream, error) {
	if len(id) > 0 {
		return u.seriesStreams[id[0]], nil
	}
	result := make([]provider.SeriesStream, 0, u.seriesCount)
	for _, v := range u.seriesStreams {
		result = append(result, v...)
	}
	return result, nil
}

func (u *defaultUser) VODInfo(id string) ([]byte, error) {
	return u.upstream.VODInfo(id)
}

func (u *defaultUser) SeriesInfo(id string) ([]byte, error) {
	return u.upstream.SeriesInfo(id)
}

func (u *defaultUser) EPGShort(id string, limit ...int) ([]byte, error) {
	return u.upstream.EPGShort(id, limit...)
}

func (u *defaultUser) EPGFull() ([]byte, error) {
	return u.epg, nil
}

func (u *defaultUser) EPGAll(streamID string) ([]byte, error) {
	return u.upstream.EPGAll(streamID)
}

func countMapVals[T provider.LiveStream | provider.SeriesStream | provider.VODStream](m map[string][]T) uint64 {
	c := uint64(0)
	for _, v := range m {
		c += uint64(len(v))
	}
	return c
}
