package users

import (
	"context"

	"gitlab.com/ddb_db/xtreamhome/internal/provider"
	"gitlab.com/ddb_db/xtreamhome/internal/workers/refresh"
)

func NewPassthruUser(p provider.Provider) User {
	return passthruUser{
		p: p,
	}
}

type passthruUser struct {
	p provider.Provider
}

func (u passthruUser) Init(ctx context.Context, listeners ...refresh.RefreshSignalSender) error {
	for _, l := range listeners {
		l <- struct{}{}
	}
	return nil
}

func (u passthruUser) Auth() (provider.ServiceDetails, error) {
	return u.p.Auth()
}

func (u passthruUser) LiveCategories() ([]provider.LiveCategory, error) {
	return u.p.LiveCategories()
}

func (u passthruUser) VODCategories() ([]provider.VODCategory, error) {
	return u.p.VODCategories()
}

func (u passthruUser) SeriesCategories() ([]provider.SeriesCategory, error) {
	return u.p.SeriesCategories()
}

func (u passthruUser) LiveStreams(cat ...string) ([]provider.LiveStream, error) {
	return u.p.LiveStreams(cat...)
}

func (u passthruUser) VODStreams(cat ...string) ([]provider.VODStream, error) {
	return u.p.VODStreams(cat...)
}

func (u passthruUser) SeriesStreams(cat ...string) ([]provider.SeriesStream, error) {
	return u.p.SeriesStreams(cat...)
}

func (u passthruUser) VODInfo(id string) ([]byte, error) {
	return u.p.VODInfo(id)
}

func (u passthruUser) SeriesInfo(id string) ([]byte, error) {
	return u.p.SeriesInfo(id)
}

func (u passthruUser) EPGShort(id string, time ...int) ([]byte, error) {
	return u.p.EPGShort(id, time...)
}

func (u passthruUser) EPGAll(id string) ([]byte, error) {
	return u.p.EPGAll(id)
}

func (u passthruUser) EPGFull() ([]byte, error) {
	return u.p.EPGFull()
}

func (u passthruUser) EPGXML() (*provider.XMLTV, error) {
	return u.p.EPGXML()
}
