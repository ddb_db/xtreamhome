package users

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"log/slog"
	"sync"
	"time"

	"gitlab.com/ddb_db/xtreamhome/internal/config"
	"gitlab.com/ddb_db/xtreamhome/internal/provider"
	"gitlab.com/ddb_db/xtreamhome/internal/timerlog"
	"gitlab.com/ddb_db/xtreamhome/internal/workers/refresh"
)

var ErrNoSuchUser = errors.New("users: no such user")

func New(refreshOn refresh.RefreshSignalReceiver) Users {
	if ActiveUsers != nil {
		panic("users: already created")
	}

	ActiveUsers = &defaultUsers{
		mu:        sync.RWMutex{},
		refreshOn: refreshOn,
		users:     make(map[string]User),
		initDone:  false,
	}
	return ActiveUsers
}

type Users interface {
	Init(ctx context.Context, users []config.UserConfig, provider provider.Provider, listeners ...chan<- struct{}) error
	Get(username string) (User, error)
}

type defaultUsers struct {
	mu        sync.RWMutex
	refreshOn refresh.RefreshSignalReceiver
	upstream  provider.Provider
	users     map[string]User
	initDone  bool
}

func (d *defaultUsers) Init(ctx context.Context, users []config.UserConfig, upstream provider.Provider, listeners ...chan<- struct{}) error {
	if d.initDone {
		panic("users: already initialized")
	}

	go func() {
		slog.Info("users: started")
		for {
			select {
			case <-ctx.Done():
				slog.Warn("users: shutting down", "cause", ctx.Err().Error())
				return
			case <-d.refreshOn:
				slog.Info("users: refresh signal received")
				func() {
					d.mu.Lock()
					defer d.mu.Unlock()

					d.upstream = upstream
					live, err := d.upstream.LiveCategories()
					if err != nil {
						slog.Error("users: upstream live cats failed", "err", err.Error())
						return
					}

					vod, err := d.upstream.VODCategories()
					if err != nil {
						slog.Error("users: upstream vod cats failed", "err", err.Error())
						return
					}

					series, err := d.upstream.SeriesCategories()
					if err != nil {
						slog.Error("users: upstream series cats failed", "err", err.Error())
						return
					}

					xml, err := d.upstream.EPGXML()
					if err != nil {
						slog.Error("users: upstream epg xml failed", "err", err.Error())
						return
					}

					liveStreams, err := d.upstream.LiveStreams()
					if err != nil {
						slog.Error("users: upstream live streams failed", "err", err.Error())
						return
					}

					vodStreams, err := d.upstream.VODStreams()
					if err != nil {
						slog.Error("users: upstream vod streams failed", "err", err.Error())
						return
					}

					seriesStreams, err := d.upstream.SeriesStreams()
					if err != nil {
						slog.Error("users: upstream series streams failed", "err", err.Error())
					}

					for _, u := range users {
						filteredLiveGrps := filterLiveGroups(u.LiveGroups, live)
						epg, err := d.filterEPG(filteredLiveGrps, xml)
						if err != nil {
							slog.Error("users: epg filter failed", "err", err.Error())
							epg, _ = d.upstream.EPGFull()
						}
						filteredVODGrps := filterVODGroups(u.VODGroups, vod)
						filteredSeriesGrps := filterSeriesGroups(u.SeriesGroups, series)

						d.users[u.Username] = NewUser(
							u.Username,
							u.Password,
							filteredLiveGrps,
							filterLiveStreams(filteredLiveGrps, liveStreams),
							filteredVODGrps,
							filterVODStreams(filteredVODGrps, vodStreams),
							filteredSeriesGrps,
							filterSeriesStreams(filteredSeriesGrps, seriesStreams),
							epg,
							d.upstream,
						)
					}
				}()
				for _, l := range listeners {
					l <- struct{}{}
				}
				slog.Info("users: refresh completed")
			}
		}
	}()
	return nil
}

func (d *defaultUsers) Get(username string) (User, error) {
	d.mu.RLock()
	defer d.mu.RUnlock()

	u, ok := d.users[username]
	if !ok {
		return nil, ErrNoSuchUser
	}
	return u, nil
}

type User interface {
	provider.Provider
}

var ActiveUsers Users

func (d *defaultUsers) filterEPG(groups []provider.LiveCategory, epg *provider.XMLTV) ([]byte, error) {
	defer timerlog.Done("xml filter", time.Now())

	result := provider.XMLTV{
		Programs:          make([]provider.Program, 0, len(epg.Programs)/10),
		Channels:          make([]provider.Channel, 0, len(epg.Channels)/10),
		Date:              epg.Date,
		SourceInfoURL:     epg.SourceInfoURL,
		SourceInfoName:    epg.SourceInfoName,
		SourceDataURL:     epg.SourceDataURL,
		GeneratorInfoName: epg.GeneratorInfoName,
		GeneratorInfoURL:  epg.GeneratorInfoURL,
	}

	keys := map[string]struct{}{}
	for _, g := range groups {
		streams, err := d.upstream.LiveStreams(g.ID)
		if err != nil {
			slog.Error("live stream fetch failed", "err", err.Error())
		}
		for _, s := range streams {
			keys[s.EPGChannelID] = struct{}{}
		}
	}

	for _, c := range epg.Channels {
		if _, ok := keys[c.ID]; ok {
			result.Channels = append(result.Channels, c)
		}
	}

	for _, p := range epg.Programs {
		if _, ok := keys[p.ChannelID]; ok {
			result.Programs = append(result.Programs, p)
		}
	}

	buff := bytes.Buffer{}
	err := xml.NewEncoder(&buff).Encode(result)
	return buff.Bytes(), err
}

// TODO generics for these filters

func filterLiveGroups(filter []config.UserGroupConfig, groups []provider.LiveCategory) []provider.LiveCategory {
	if len(filter) == 0 {
		return groups
	}

	keys := map[string]provider.LiveCategory{}
	for _, f := range groups {
		keys[f.Name] = f
	}

	result := make([]provider.LiveCategory, 0, len(keys))
	for _, f := range filter {
		if g, ok := keys[f.Name]; ok {
			if f.Rename != "" {
				g.Name = f.Rename
			}
			result = append(result, g)
		}
	}
	return result
}

func filterVODGroups(filter []config.UserGroupConfig, groups []provider.VODCategory) []provider.VODCategory {
	if len(filter) == 0 {
		return groups
	}

	keys := map[string]provider.VODCategory{}
	for _, f := range groups {
		keys[f.Name] = f
	}

	result := make([]provider.VODCategory, 0, len(keys))
	for _, f := range filter {
		if g, ok := keys[f.Name]; ok {
			if f.Rename != "" {
				g.Name = f.Rename
			}
			result = append(result, g)
		}
	}
	return result
}

func filterSeriesGroups(filter []config.UserGroupConfig, groups []provider.SeriesCategory) []provider.SeriesCategory {
	if len(filter) == 0 {
		return groups
	}

	keys := map[string]provider.SeriesCategory{}
	for _, f := range groups {
		keys[f.Name] = f
	}

	result := make([]provider.SeriesCategory, 0, len(keys))
	for _, f := range filter {
		if g, ok := keys[f.Name]; ok {
			if f.Rename != "" {
				g.Name = f.Rename
			}
			result = append(result, g)
		}
	}
	return result
}

func filterSeriesStreams(cats []provider.SeriesCategory, streams []provider.SeriesStream) map[string][]provider.SeriesStream {
	result := map[string][]provider.SeriesStream{}
	for _, c := range cats {
		result[c.ID] = make([]provider.SeriesStream, 0, 100)
	}

	for _, s := range streams {
		if l, ok := result[s.CategoryID]; ok {
			result[s.CategoryID] = append(l, s)
		}
	}
	return result
}

func filterVODStreams(cats []provider.VODCategory, streams []provider.VODStream) map[string][]provider.VODStream {
	result := map[string][]provider.VODStream{}
	for _, c := range cats {
		result[c.ID] = make([]provider.VODStream, 0, 100)
	}

	for _, s := range streams {
		if l, ok := result[s.CategoryID]; ok {
			result[s.CategoryID] = append(l, s)
		}
	}
	return result
}

func filterLiveStreams(cats []provider.LiveCategory, streams []provider.LiveStream) map[string][]provider.LiveStream {
	result := map[string][]provider.LiveStream{}
	for _, c := range cats {
		result[c.ID] = make([]provider.LiveStream, 0, 100)
	}

	for _, s := range streams {
		if l, ok := result[s.CategoryID]; ok {
			result[s.CategoryID] = append(l, s)
		}
	}
	return result
}
