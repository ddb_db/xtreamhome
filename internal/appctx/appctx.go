package appctx

type AppCtxKey string

const CtxUsernameKey AppCtxKey = "username"
const CtxUserProviderKey AppCtxKey = "provider"
