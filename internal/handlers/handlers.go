package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"strings"

	"gitlab.com/ddb_db/xtreamhome/internal/config"
)

func RedirectHandler(w http.ResponseWriter, r *http.Request) {
	cfg := config.ActiveConfig
	suffix := strings.SplitN(r.URL.Path, "/", 5)
	target := fmt.Sprintf("%s/%s/%s/%s", cfg.Provider.URL, cfg.Provider.Username, cfg.Provider.Password, suffix[4])
	slog.Info("Redirect", "target", target)
	http.Redirect(w, r, target, http.StatusTemporaryRedirect)
}

func Handler404(w http.ResponseWriter, r *http.Request) {
	WriteJSONError(http.StatusNotFound, errors.New("resource not found"), w)
}

func WriteJSON(status int, v any, w http.ResponseWriter) {
	w.Header().Add("Content-Type", "application/json")
	enc, err := json.Marshal(v)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		ev, ee := json.Marshal(APIError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
		if ee != nil {
			panic(ee)
		}
		w.Write(ev)
	}
	w.WriteHeader(status)
	if _, err := w.Write(enc); err != nil {
		panic(err)
	}
}

func WriteJSONRaw(status int, b []byte, w http.ResponseWriter) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
	if _, err := w.Write(b); err != nil {
		panic(err)
	}
}

func WriteJSONError(status int, err error, w http.ResponseWriter) {
	WriteJSON(status, APIError{
		Code:    status,
		Message: err.Error(),
	}, w)
}
