package handlers

import (
	"log/slog"
	"net/http"

	"gitlab.com/ddb_db/xtreamhome/internal/appctx"
	"gitlab.com/ddb_db/xtreamhome/internal/provider"
)

func getXMLTVHandler(w http.ResponseWriter, r *http.Request) {
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)
	resp, err := p.EPGFull()
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	w.Header().Add("Content-Type", "application/xml")
	if _, err := w.Write(resp); err != nil {
		slog.Error("handlers(xmltv): io error", "err", err.Error())
		panic(err)
	}
}

func getServiceDetailsHandler(w http.ResponseWriter, r *http.Request) {
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)
	resp, err := p.Auth()
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSON(http.StatusOK, resp, w)
}

func getLiveCategoriesHandler(w http.ResponseWriter, r *http.Request) {
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)
	resp, err := p.LiveCategories()
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSON(http.StatusOK, resp, w)
}

func getVODCategoriesHandler(w http.ResponseWriter, r *http.Request) {
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)
	resp, err := p.VODCategories()
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSON(http.StatusOK, resp, w)
}

func getSeriesCategoriesHandler(w http.ResponseWriter, r *http.Request) {
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)
	resp, err := p.SeriesCategories()
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSON(http.StatusOK, resp, w)
}

func getLiveStreamsHandler(w http.ResponseWriter, r *http.Request) {
	args := make([]string, 0, 1)
	catID := r.URL.Query().Get("category_id")
	if catID != "" {
		args = append(args, catID)
	}
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)

	resp, err := p.LiveStreams(args...)
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSON(http.StatusOK, resp, w)
}

func getVODStreamsHandler(w http.ResponseWriter, r *http.Request) {
	args := make([]string, 0, 1)
	catID := r.URL.Query().Get("category_id")
	if catID != "" {
		args = append(args, catID)
	}
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)

	resp, err := p.VODStreams(args...)
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSON(http.StatusOK, resp, w)
}

func getSeriesStreamsHandler(w http.ResponseWriter, r *http.Request) {
	args := make([]string, 0, 1)
	catID := r.URL.Query().Get("category_id")
	if catID != "" {
		args = append(args, catID)
	}
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)

	resp, err := p.SeriesStreams(args...)
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSON(http.StatusOK, resp, w)
}

func getSeriesInfoHandler(w http.ResponseWriter, r *http.Request) {
	seriesID := r.URL.Query().Get("series_id")
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)

	resp, err := p.SeriesInfo(seriesID)
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSONRaw(http.StatusOK, resp, w)
}

func getVODInfoHandler(w http.ResponseWriter, r *http.Request) {
	vodID := r.URL.Query().Get("vod_id")
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)

	resp, err := p.VODInfo(vodID)
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSONRaw(http.StatusOK, resp, w)
}

func getShortEPGHandler(w http.ResponseWriter, r *http.Request) {
	streamID := r.URL.Query().Get("stream_id")
	// TODO handle limit param
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)

	resp, err := p.EPGShort(streamID)
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSONRaw(http.StatusOK, resp, w)
}

func getAllEPGHandler(w http.ResponseWriter, r *http.Request) {
	streamID := r.URL.Query().Get("stream_id")
	p := r.Context().Value(appctx.CtxUserProviderKey).(provider.Provider)

	resp, err := p.EPGAll(streamID)
	if err != nil {
		WriteJSONError(http.StatusInternalServerError, err, w)
		return
	}
	WriteJSONRaw(http.StatusOK, resp, w)
}
