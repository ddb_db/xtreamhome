package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
)

func CommandHandler(w http.ResponseWriter, r *http.Request) {
	cmd := mux.Vars(r)["cmd"]

	switch cmd {
	case "player_api.php":
		action := r.URL.Query().Get("action")
		getHandlerForAction(action)(w, r)
	case "xmltv.php":
		getXMLTVHandler(w, r)
	default:
		Handler404(w, r)
	}
}

func getHandlerForAction(action string) http.HandlerFunc {
	var h http.HandlerFunc
	switch action {
	case "":
		h = getServiceDetailsHandler
	case "get_live_categories":
		h = getLiveCategoriesHandler
	case "get_vod_categories":
		h = getVODCategoriesHandler
	case "get_series_categories":
		h = getSeriesCategoriesHandler
	case "get_live_streams":
		h = getLiveStreamsHandler
	case "get_vod_streams":
		h = getVODStreamsHandler
	case "get_series":
		h = getSeriesStreamsHandler
	case "get_series_info":
		h = getSeriesInfoHandler
	case "get_vod_info":
		h = getVODInfoHandler
	case "get_short_epg":
		h = getShortEPGHandler
	case "get_simple_data_table":
		h = getAllEPGHandler
	default:
		h = Handler404
	}
	return h
}
