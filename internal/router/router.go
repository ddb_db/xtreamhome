package router

import (
	"net/http"
	"net/http/pprof"

	"log/slog"

	"github.com/gorilla/mux"
	"gitlab.com/ddb_db/xtreamhome/internal/config"
	"gitlab.com/ddb_db/xtreamhome/internal/handlers"
	"gitlab.com/ddb_db/xtreamhome/internal/middleware/auth"
	"gitlab.com/ddb_db/xtreamhome/internal/middleware/logger"
	"gitlab.com/ddb_db/xtreamhome/internal/users"
)

type Option func(defaultImpl)

func WithConfig(cfg config.Config) Option {
	return func(impl defaultImpl) {
		impl.cfg = &cfg
	}
}

func WithUsers(u users.Users) Option {
	return func(impl defaultImpl) {
		impl.usrs = u
	}
}

func WithAuthHandler(a auth.AuthHandler) Option {
	return func(impl defaultImpl) {
		impl.auth = a
	}
}

func New(opts ...Option) *mux.Router {
	bld := defaultImpl{}
	for _, o := range opts {
		o(bld)
	}

	if bld.cfg == nil {
		bld.cfg = config.ActiveConfig
	}

	if bld.usrs == nil {
		bld.usrs = users.ActiveUsers
	}

	if bld.auth == nil {
		bld.auth = auth.NewAuthHandler(*bld.cfg, bld.usrs)
	}

	return bld.build()
}

type defaultImpl struct {
	cfg  *config.Config
	usrs users.Users
	auth auth.AuthHandler
}

func (impl defaultImpl) build() *mux.Router {
	r := mux.NewRouter()
	impl.setReqLogging(r)
	impl.setPprof(r)

	qryRouter := r.Path("/{cmd}").Subrouter()
	qryRouter.Use(impl.auth.QueryHandler)
	qryRouter.NewRoute().Handler(http.HandlerFunc(handlers.CommandHandler))

	pathRouter := r.PathPrefix(`/{type:live|movie|series|timeshift}/{id}/{pwd}/`).Subrouter()
	pathRouter.Use(impl.auth.PathHandler)
	pathRouter.NewRoute().Handler(http.HandlerFunc(handlers.RedirectHandler))

	r.PathPrefix("/").Handler(http.HandlerFunc(handlers.Handler404))

	return r
}

func (impl defaultImpl) setReqLogging(r *mux.Router) {
	if !impl.cfg.Config.LogReqs {
		return
	}

	r.Use(logger.New(slog.Default()))
	slog.Debug("request logging enabled")
}

func (impl defaultImpl) setPprof(r *mux.Router) {
	if !impl.cfg.Config.EnablePprof {
		return
	}

	r.HandleFunc("/debug/pprof/", pprof.Index)
	slog.Warn("UNSECURED PPROF ENDPOINT EXPOSED!")
}
