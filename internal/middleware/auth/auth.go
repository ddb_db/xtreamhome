package auth

import (
	"context"
	"errors"
	"net/http"
	"regexp"
	"strings"

	"gitlab.com/ddb_db/xtreamhome/internal/appctx"
	"gitlab.com/ddb_db/xtreamhome/internal/config"
	"gitlab.com/ddb_db/xtreamhome/internal/handlers"
	"gitlab.com/ddb_db/xtreamhome/internal/users"
)

var pathCredsRegex = regexp.MustCompile(`^\/(?:live|movies|series|timeshift)\/.+`)

type AuthHandler interface {
	QueryHandler(http.Handler) http.Handler
	PathHandler(http.Handler) http.Handler
}

func NewAuthHandler(cfg config.Config, usersSvc users.Users) AuthHandler {
	return &authHandler{
		cfg:      cfg,
		usersSvc: usersSvc,
	}
}

type authHandler struct {
	cfg      config.Config
	usersSvc users.Users
}

func (h authHandler) QueryHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		qry := r.URL.Query()
		username := qry.Get("username")
		password := qry.Get("password")
		h.verifyCreds(username, password, next, w, r)
	}
	return http.HandlerFunc(fn)
}

func (h authHandler) PathHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		if !pathCredsRegex.Match([]byte(r.URL.Path)) {
			next.ServeHTTP(w, r)
			return
		}
		parts := strings.Split(r.URL.Path, "/")
		username := parts[2]
		password := parts[3]
		h.verifyCreds(username, password, next, w, r)
	}
	return http.HandlerFunc(fn)
}

func (h authHandler) verifyCreds(username, password string, next http.Handler, w http.ResponseWriter, r *http.Request) {
	if checkCreds(username, password, h.cfg.Users) {
		req := r.WithContext(context.WithValue(r.Context(), appctx.CtxUsernameKey, username))
		p, err := h.usersSvc.Get(username)
		if err != nil {
			handlers.WriteJSONError(http.StatusInternalServerError, err, w)
			return
		}
		req = r.WithContext(context.WithValue(req.Context(), appctx.CtxUserProviderKey, p))
		next.ServeHTTP(w, req)
		return
	}
	handlers.WriteJSONError(http.StatusForbidden, errors.New("unauthorized"), w)
}

func checkCreds(username, password string, users []config.UserConfig) bool {
	for _, u := range users {
		if u.Username == username && u.Password == password {
			return true
		}
	}
	return false
}
