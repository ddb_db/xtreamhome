package timerlog

import (
	"fmt"
	"log/slog"
	"time"
)

func Done(lbl string, start time.Time) {
	slog.Info("timerlog: done", "d", time.Since(start).String(), "lbl", fmt.Sprintf("[%s]", lbl))
}
