package provider

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"

	"gitlab.com/ddb_db/xtreamhome/internal/workers/refresh"
)

type Provider interface {
	Init(context.Context, ...refresh.RefreshSignalSender) error
	Auth() (ServiceDetails, error)
	LiveCategories() ([]LiveCategory, error)
	VODCategories() ([]VODCategory, error)
	SeriesCategories() ([]SeriesCategory, error)
	LiveStreams(...string) ([]LiveStream, error)
	VODStreams(...string) ([]VODStream, error)
	SeriesStreams(...string) ([]SeriesStream, error)
	VODInfo(string) ([]byte, error)
	SeriesInfo(string) ([]byte, error)
	EPGShort(string, ...int) ([]byte, error)
	EPGAll(string) ([]byte, error)
	EPGFull() ([]byte, error)
	EPGXML() (*XMLTV, error)
}

type UserDetails struct {
	Username             string   `json:"username"`
	Password             string   `json:"password"`
	Message              string   `json:"message"`
	Auth                 int      `json:"auth"`
	Status               string   `json:"status"`
	ExpireTimestamp      string   `json:"exp_date"`
	IsTrial              string   `json:"is_trial"`
	ActiveConnections    string   `json:"active_cons"`
	CreateTimestamp      string   `json:"created_at"`
	MaxConnections       string   `json:"max_connections"`
	AllowedOutputFormats []string `json:"allowed_output_formats"`
}

type ServerDetails struct {
	URL            string `json:"url"`
	Port           int    `json:"port"`
	HTTPSPort      string `json:"https_port"`
	ServerProtocol string `json:"server_protocol"`
	RTMPPort       string `json:"rtmp_port"`
	Timezone       string `json:"timezone"`
	NowTimestamp   uint64 `json:"timestamp_now"`
	Now            string `json:"time_now"`
	Process        bool   `json:"process"`
}

type ServiceDetails struct {
	UserInfo   UserDetails   `json:"user_info"`
	ServerInfo ServerDetails `json:"server_info"`
}

type Category struct {
	ID       string `json:"category_id"`
	Name     string `json:"category_name"`
	ParentID int    `json:"parent_id"`
}

type LiveCategory Category

type VODCategory Category

type SeriesCategory Category

type Stream struct {
	Index        int    `json:"num"`
	Name         string `json:"name"`
	StreamType   string `json:"stream_type"`
	Added        string `json:"added"`
	IsAdult      string `json:"is_adult"`
	CategoryID   string `json:"category_id"`
	CustomSID    string `json:"custom_sid"`
	TVArchive    int    `json:"tv_archive"`
	DirectSource string `json:"direct_source"`
}

type LiveStream struct {
	Stream
	ID                int    `json:"stream_id"`
	Icon              string `json:"stream_icon"`
	EPGChannelID      string `json:"epg_channel_id"`
	TVArchiveDuration int    `json:"tv_archive_duration"`
}

type VODStream struct {
	Stream
	ID                 int     `json:"stream_id"`
	Icon               string  `json:"stream_icon"`
	Rating             string  `json:"rating"`
	StarRating         float32 `json:"rating_5based"`
	ContainerExtension string  `json:"container_extension"`
}

type mixedString string

func (s mixedString) UnmarshalJSON(in []byte) error {
	var v any
	if err := json.Unmarshal(in, &v); err != nil {
		return err
	}
	switch v := v.(type) {
	case string:
		s = mixedString(v)
		return nil
	case float64:
		s = mixedString(strconv.FormatFloat(v, 'f', 0, 64))
		return nil
	default:
		panic(fmt.Errorf("unknown type: %s", reflect.TypeOf(v)))
	}
}

type SeriesStream struct {
	Stream
	ID             int         `json:"series_id"`
	Cover          string      `json:"cover"`
	Plot           string      `json:"plot"`
	Cast           string      `json:"cast"`
	Director       string      `json:"director"`
	Genre          string      `json:"genre"`
	ReleaseDate    string      `json:"releaseDate"`
	LastModified   string      `json:"last_modified"`
	BackdropPath   []string    `json:"backdrop_path"`
	YoutubeTrailer string      `json:"youtube_trailer"`
	EpisodeRuntime mixedString `json:"episode_run_time"`
	Rating         float32     `json:"rating"`
	StarRating     float32     `json:"rating_5based"`
}

var ActiveProvider Provider
