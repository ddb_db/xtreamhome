package provider

import "encoding/xml"

type XMLTV struct {
	XMLName           xml.Name  `xml:"tv"`
	Date              string    `xml:"date,attr,omitempty"`
	SourceInfoURL     string    `xml:"source-info-url,attr,omitempty"`
	SourceInfoName    string    `xml:"source-info-name,attr,omitempty"`
	SourceDataURL     string    `xml:"source-data-url,attr,omitempty"`
	GeneratorInfoName string    `xml:"generator-info-name,attr,omitempty"`
	GeneratorInfoURL  string    `xml:"generator-info-url,attr,omitempty"`
	Channels          []Channel `xml:"channel"` // note: tivimate expects channels before programs in the xml file
	Programs          []Program `xml:"programme"`
}

type Program struct {
	XMLName   xml.Name `xml:"programme"`
	ChannelID string   `xml:"channel,attr"`
	Start     string   `xml:"start,attr"`
	Stop      string   `xml:"stop,attr"`
	PDCStart  string   `xml:"pdc-start,attr,omitempty"`
	VPSStart  string   `xml:"vps-start,attr,omitempty"`
	ShowView  string   `xml:"showview,attr,omitempty"`
	VideoPlus string   `xml:"videoplus,attr,omitempty"`
	ClumpIDX  string   `xml:"clumpidx,attr,omitempty"`
	Blob      string   `xml:",innerxml"`
}

type Channel struct {
	XMLName xml.Name `xml:"channel"`
	ID      string   `xml:"id,attr"`
	Blob    string   `xml:",innerxml"`
}
