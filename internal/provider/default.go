package provider

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/jellydator/ttlcache/v3"
	"gitlab.com/ddb_db/xtreamhome/internal/config"
	"gitlab.com/ddb_db/xtreamhome/internal/timerlog"
	"gitlab.com/ddb_db/xtreamhome/internal/workers/refresh"
)

const (
	keyCatsLive      = "cats-live"
	keyCatsVOD       = "cats-vod"
	keyCatsSeries    = "cats-series"
	keyEPGAll        = "epg-all"
	keyEPGFull       = "epg-full"
	keyEPGShort      = "epg-short"
	keyEPGXML        = "epg-xml"
	keyInfoSeries    = "info-series"
	keyInfoVOD       = "info-vod"
	keyStreamsLive   = "streams-live"
	keyStreamsSeries = "streams-series"
	keyStreamsVOD    = "stream-vod"
)

func New(username, password, url string, refresh refresh.RefreshSignal) Provider {
	if ActiveProvider != nil {
		panic("provider: already initialized")
	}

	if refresh == nil {
		panic("provider: refresh signal cannot be nil")
	}

	ActiveProvider = &defaultImpl{
		URL:      url,
		Username: username,
		Password: password,
		httpc:    http.Client{},
		refresh:  refresh,
		cache: ttlcache.New[string, any](
			ttlcache.WithDisableTouchOnHit[string, any](),
			ttlcache.WithCapacity[string, any](uint64(config.ActiveConfig.Provider.Cache)),
		),
		doneInit: false,
	}

	return ActiveProvider
}

type defaultImpl struct {
	URL      string
	Username string
	Password string
	refresh  refresh.RefreshSignal
	httpc    http.Client
	cache    *ttlcache.Cache[string, any]
	doneInit bool
}

func (impl *defaultImpl) doAction(cmd string, args url.Values) ([]byte, error) {
	if args == nil {
		args = url.Values{}
	}
	args.Set("action", cmd)
	r, err := impl.do("player_api.php", args)
	if r != nil {
		defer r.Close()
	}
	if err != nil {
		return nil, err
	}
	return io.ReadAll(r)
}

// caller is responsible for closing this reader
func (impl *defaultImpl) doXML() (io.ReadCloser, error) {
	return impl.do("xmltv.php", nil)
}

// caller is responsible for closing the returned reader
func (impl *defaultImpl) do(path string, args url.Values) (io.ReadCloser, error) {
	if args == nil {
		args = url.Values{}
	}
	args.Set("username", impl.Username)
	args.Set("password", impl.Password)

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/%s", impl.URL, path), nil)
	if err != nil {
		return nil, err
	}
	req.URL.RawQuery = args.Encode()

	resp, err := impl.httpc.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		resp.Body.Close()
		return nil, fmt.Errorf("http error [%s]", resp.Status)
	}
	return resp.Body, nil
}

func (impl *defaultImpl) Init(ctx context.Context, listeners ...refresh.RefreshSignalSender) error {
	if impl.doneInit {
		panic("provider(default): already initialized")
	}

	go impl.cache.Start()
	go func() {
		for {
			select {
			case <-ctx.Done():
				slog.Info("provider(default): terminating", "cause", ctx.Err().Error())
				return
			case <-impl.refresh:
				func() {
					initErrs := make([]error, 0)
					errs := make(chan error)
					errCheckDone := make(chan struct{})
					wg := sync.WaitGroup{}
					wg.Add(4)

					go func() {
						defer wg.Done()

						var err error
						var resp []byte
						func() {
							defer timerlog.Done("xml download", time.Now())
							var rc io.ReadCloser
							rc, err = impl.doXML()
							if err == nil {
								defer rc.Close()
								resp, err = io.ReadAll(rc)
							}
						}()
						if err != nil {
							errs <- fmt.Errorf("provider(default): xmltv failed: %w", err)
							return
						}
						impl.cache.Set(keyEPGFull, resp, ttlcache.NoTTL)

						func() {
							defer timerlog.Done("xml parse", time.Now())
							var xmltv XMLTV
							d := xml.NewDecoder(bytes.NewReader(resp))
							err := d.Decode(&xmltv)
							if err != nil {
								slog.Warn("provider(default): xml parse failed, partial xml data", "err", err.Error())
							}
							impl.cache.Set(keyEPGXML, &xmltv, ttlcache.NoTTL)
						}()

					}()

					go func() {
						defer timerlog.Done("live categories", time.Now())
						defer wg.Done()
						resp, err := impl.doAction("get_live_categories", nil)
						if err != nil {
							errs <- fmt.Errorf("provider(default): command failed: %w", err)
							return
						}
						impl.cache.Set(keyCatsLive, resp, ttlcache.NoTTL)
					}()

					go func() {
						defer timerlog.Done("vod categories", time.Now())
						defer wg.Done()
						resp, err := impl.doAction("get_vod_categories", nil)
						if err != nil {
							errs <- fmt.Errorf("provider(default): command failed: %w", err)
							return
						}
						impl.cache.Set(keyCatsVOD, resp, ttlcache.NoTTL)
					}()

					go func() {
						defer timerlog.Done("series categories", time.Now())
						defer wg.Done()
						resp, err := impl.doAction("get_series_categories", nil)
						if err != nil {
							errs <- fmt.Errorf("provider(default): command failed: %w", err)
							return
						}
						impl.cache.Set(keyCatsSeries, resp, ttlcache.NoTTL)
					}()

					go func() {
						for err := range errs {
							initErrs = append(initErrs, err)
						}
						errCheckDone <- struct{}{}
					}()

					wg.Wait()
					close(errs)
					<-errCheckDone
					close(errCheckDone)

					if !impl.doneInit && len(initErrs) == 0 {
						impl.doneInit = true
						slog.Info("provider(default): ready")
					} else if !impl.doneInit && len(initErrs) > 0 {
						for _, e := range initErrs {
							slog.Error(e.Error())
						}
						panic("provider(default): init failed")
					} else if len(initErrs) > 0 {
						for _, e := range initErrs {
							slog.Error(e.Error())
						}
						slog.Error("provider(default): refresh failed")
					} else {
						slog.Info("provider(default): refresh completed")
					}
					for _, l := range listeners {
						l <- struct{}{}
					}
				}()
			}
		}
	}()

	impl.refresh <- struct{}{}
	return nil
}

func (impl *defaultImpl) EPGXML() (*XMLTV, error) {
	item := impl.cache.Get(keyEPGXML)
	if item != nil {
		return item.Value().(*XMLTV), nil
	}
	return nil, errors.New("provider(default): no xmltv found in cache")
}

func (impl *defaultImpl) Auth() (ServiceDetails, error) {
	resp, err := impl.doAction("", nil)
	if err != nil {
		return ServiceDetails{}, fmt.Errorf("provider(default): auth failed: %w", err)
	}
	var sd ServiceDetails
	if err := json.Unmarshal(resp, &sd); err != nil {
		return ServiceDetails{}, fmt.Errorf("provider(default): auth unmarshal failed: %w", err)
	}
	return sd, nil
}

func (impl *defaultImpl) LiveCategories() ([]LiveCategory, error) {
	item := impl.cache.Get(keyCatsLive)
	if item != nil {
		var resp []LiveCategory
		if err := json.Unmarshal(item.Value().([]byte), &resp); err != nil {
			return nil, fmt.Errorf("provider(default): live cats unmarshal failed: %w", err)
		}
		return resp, nil
	}
	return nil, errors.New("provider(default): no live cats found in cache")
}

func (impl *defaultImpl) VODCategories() ([]VODCategory, error) {
	item := impl.cache.Get(keyCatsVOD)
	if item != nil {
		var resp []VODCategory
		if err := json.Unmarshal(item.Value().([]byte), &resp); err != nil {
			return nil, fmt.Errorf("provider(default): vod cats unmarshal failed: %w", err)
		}
		return resp, nil
	}
	return nil, errors.New("provider(default): no vod cats found in cache")
}

func (impl *defaultImpl) SeriesCategories() ([]SeriesCategory, error) {
	item := impl.cache.Get(keyCatsSeries)
	if item != nil {
		var resp []SeriesCategory
		if err := json.Unmarshal(item.Value().([]byte), &resp); err != nil {
			return nil, fmt.Errorf("provider(default): series cats unmarshal failed: %w", err)
		}
		return resp, nil
	}
	return nil, errors.New("provider(default): no series cats found in cache")
}

func (impl *defaultImpl) LiveStreams(id ...string) ([]LiveStream, error) {
	if len(id) > 1 {
		panic("one arg max")
	}

	item := impl.cache.Get(keyStreamsLive)
	if item == nil {
		resp, err := impl.doAction("get_live_streams", nil)
		if err != nil {
			return nil, fmt.Errorf("provider(default): get live streams failed: %w", err)
		}
		var streams []LiveStream
		if err := json.Unmarshal(resp, &streams); err != nil {
			return nil, fmt.Errorf("provider(default): live streams unmarshal failed: %w", err)
		}
		impl.cache.Set(keyStreamsLive, streams, config.ActiveConfig.Provider.CacheTTL)
		item = impl.cache.Get(keyStreamsLive)
	}

	streams := item.Value().([]LiveStream)
	if len(id) == 0 {
		return streams, nil
	}

	filtered := make([]LiveStream, 0, len(streams)/5)
	for _, s := range streams {
		if s.CategoryID == id[0] {
			filtered = append(filtered, s)
		}
	}
	return filtered, nil
}

func (impl *defaultImpl) VODStreams(id ...string) ([]VODStream, error) {
	if len(id) > 1 {
		panic("one arg max")
	}

	item := impl.cache.Get(keyStreamsVOD)
	if item == nil {
		resp, err := impl.doAction("get_vod_streams", nil)
		if err != nil {
			return nil, fmt.Errorf("provider(default): get vod streams failed: %w", err)
		}
		var streams []VODStream
		if err := json.Unmarshal(resp, &streams); err != nil {
			return nil, fmt.Errorf("provider(default): vod streams unmarshal failed: %w", err)
		}
		impl.cache.Set(keyStreamsVOD, streams, config.ActiveConfig.Provider.CacheTTL)
		item = impl.cache.Get(keyStreamsVOD)
	}

	streams := item.Value().([]VODStream)
	if len(id) == 0 {
		return streams, nil
	}

	filtered := make([]VODStream, 0, len(streams)/5)
	for _, s := range streams {
		if s.CategoryID == id[0] {
			filtered = append(filtered, s)
		}
	}
	return filtered, nil
}

func (impl *defaultImpl) SeriesStreams(id ...string) ([]SeriesStream, error) {
	if len(id) > 1 {
		panic("one arg max")
	}

	item := impl.cache.Get(keyStreamsSeries)
	if item == nil {
		resp, err := impl.doAction("get_series", nil)
		if err != nil {
			return nil, fmt.Errorf("provider(default): get series streams failed: %w", err)
		}
		var streams []SeriesStream
		if err := json.Unmarshal(resp, &streams); err != nil {
			return nil, fmt.Errorf("provider(default): series streams unmarshal failed: %w", err)
		}
		impl.cache.Set(keyStreamsSeries, streams, config.ActiveConfig.Provider.CacheTTL)
		item = impl.cache.Get(keyStreamsSeries)
	}

	streams := item.Value().([]SeriesStream)
	if len(id) == 0 {
		return streams, nil
	}

	filtered := make([]SeriesStream, 0, len(streams)/5)
	for _, s := range streams {
		if s.CategoryID == id[0] {
			filtered = append(filtered, s)
		}
	}
	return filtered, nil
}

func (impl *defaultImpl) VODInfo(id string) ([]byte, error) {
	key := fmt.Sprintf("%s:%s", keyInfoVOD, id)
	item := impl.cache.Get(key)
	if item == nil {
		args := url.Values{}
		args.Set("vod_id", id)
		resp, err := impl.doAction("get_vod_info", args)
		if err != nil {
			return nil, fmt.Errorf("provider(default): vod info failed: %w", err)
		}
		impl.cache.Set(key, resp, config.ActiveConfig.Provider.CacheTTL)
		item = impl.cache.Get(key)
	}
	return item.Value().([]byte), nil
}

func (impl *defaultImpl) SeriesInfo(id string) ([]byte, error) {
	key := fmt.Sprintf("%s:%s", keyInfoSeries, id)
	item := impl.cache.Get(key)
	if item == nil {
		args := url.Values{}
		args.Set("series_id", id)
		resp, err := impl.doAction("get_series_info", args)
		if err != nil {
			return nil, fmt.Errorf("provider(default): series info failed: %w", err)
		}
		impl.cache.Set(key, resp, config.ActiveConfig.Provider.CacheTTL)
		item = impl.cache.Get(key)
	}
	return item.Value().([]byte), nil
}

func (impl *defaultImpl) EPGShort(id string, limit ...int) ([]byte, error) {
	key := fmt.Sprintf("%s:%s", keyEPGShort, id)
	item := impl.cache.Get(key)
	if item == nil {
		args := url.Values{}
		args.Set("stream_id", id)
		resp, err := impl.doAction("get_short_epg", args)
		if err != nil {
			return nil, fmt.Errorf("provider(default): epg short failed: %w", err)
		}
		impl.cache.Set(key, resp, config.ActiveConfig.Provider.CacheTTL)
		item = impl.cache.Get(key)
	}
	return item.Value().([]byte), nil
}

func (impl *defaultImpl) EPGFull() ([]byte, error) {
	item := impl.cache.Get(keyEPGFull)
	if item != nil {
		return item.Value().([]byte), nil
	}
	return nil, errors.New("provider(default): epg full not found in cache")
}

func (impl *defaultImpl) EPGAll(id string) ([]byte, error) {
	key := fmt.Sprintf("%s:%s", keyEPGAll, id)
	item := impl.cache.Get(key)
	if item == nil {
		args := url.Values{}
		args.Set("stream_id", id)
		resp, err := impl.doAction("get_simple_data_table", args)
		if err != nil {
			return nil, fmt.Errorf("provider(default): epg all failed: %w", err)
		}
		impl.cache.Set(key, resp, config.ActiveConfig.Provider.CacheTTL)
		item = impl.cache.Get(key)
	}
	return item.Value().([]byte), nil
}
