// This will NOT compile without the appropriate static json files to embed
// They are not included in git as the only ones I have are generated from my actual
// provider.  Eventually I'll generate some fake ones that I can commit.  The names
// of the files should tell you which Xtream commands you'd need to generate them
// from if you need to build this package.  This static provider is only used for
// development anyways.  If you're just trying to build this app, you could safely
// delete this file and carry on.

package provider

import (
	"context"
	_ "embed"
	"encoding/json"

	"gitlab.com/ddb_db/xtreamhome/internal/workers/refresh"
)

func NewStaticProvider() Provider {
	if ActiveProvider != nil {
		panic("provider: already initialized")
	}
	ActiveProvider = staticImpl{}
	return ActiveProvider
}

type staticImpl struct{}

func (impl staticImpl) Init(ctx context.Context, listeners ...refresh.RefreshSignalSender) error {
	go func() {
		for _, l := range listeners {
			l <- struct{}{}
		}
	}()
	return nil
}

//go:embed static/auth.json
var authData []byte

func (impl staticImpl) Auth() (ServiceDetails, error) {
	var resp ServiceDetails
	return resp, json.Unmarshal(authData, &resp)
}

//go:embed static/cats_live.json
var liveCatsData []byte

func (impl staticImpl) LiveCategories() ([]LiveCategory, error) {
	var resp []LiveCategory
	return resp, json.Unmarshal(liveCatsData, &resp)
}

//go:embed static/cats_vod.json
var vodCatsData []byte

func (impl staticImpl) VODCategories() ([]VODCategory, error) {
	var resp []VODCategory
	return resp, json.Unmarshal(vodCatsData, &resp)
}

//go:embed static/cats_series.json
var seriesCatsData []byte

func (impl staticImpl) SeriesCategories() ([]SeriesCategory, error) {
	var resp []SeriesCategory
	return resp, json.Unmarshal(seriesCatsData, &resp)
}

//go:embed static/streams_live.json
var liveStreamsData []byte

func (impl staticImpl) LiveStreams(...string) ([]LiveStream, error) {
	var resp []LiveStream
	return resp, json.Unmarshal(liveStreamsData, &resp)
}

//go:embed static/streams_vod.json
var vodStreamsData []byte

func (impl staticImpl) VODStreams(...string) ([]VODStream, error) {
	var resp []VODStream
	return resp, json.Unmarshal(vodStreamsData, &resp)
}

//go:embed static/streams_series.json
var seriesStreamsData []byte

func (impl staticImpl) SeriesStreams(...string) ([]SeriesStream, error) {
	var resp []SeriesStream
	return resp, json.Unmarshal(seriesStreamsData, &resp)
}

//go:embed static/info_vod.json
var vodInfoData []byte

func (impl staticImpl) VODInfo(string) ([]byte, error) {
	return vodInfoData, nil
}

//go:embed static/info_series.json
var seriesInfoData []byte

func (impl staticImpl) SeriesInfo(string) ([]byte, error) {
	return seriesInfoData, nil
}

//go:embed static/epg_short.json
var shortEPGData []byte

func (impl staticImpl) EPGShort(string, ...int) ([]byte, error) {
	return shortEPGData, nil
}

//go:embed static/epg_full.json
var fullEPGData []byte

func (impl staticImpl) EPGAll(string) ([]byte, error) {
	return fullEPGData, nil
}

func (impl staticImpl) EPGFull() ([]byte, error) {
	return []byte{}, nil
}

func (impl staticImpl) EPGXML() (*XMLTV, error) { return nil, nil }
